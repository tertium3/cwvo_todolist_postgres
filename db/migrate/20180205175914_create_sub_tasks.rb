class CreateSubTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :sub_tasks do |t|
      t.text :summary
      t.text :text
      t.datetime :due_date
      t.text :important
      t.references :task, foreign_key: true

      t.timestamps
    end
  end
end
