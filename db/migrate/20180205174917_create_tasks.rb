class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.text :summary
      t.text :text
      t.datetime :due_date
      t.text :important

      t.timestamps
    end
  end
end
